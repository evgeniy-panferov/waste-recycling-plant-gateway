package com.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class WasteRecyclingPlantGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(WasteRecyclingPlantGatewayApplication.class, args);
    }

}
