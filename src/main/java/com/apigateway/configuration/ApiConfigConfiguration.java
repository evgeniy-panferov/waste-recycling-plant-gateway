package com.apigateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfigConfiguration {

    @Bean
    RouteLocator customRouteLocator(RouteLocatorBuilder builder) {

        return builder.routes()
                .route("garbage-tank",
                        route -> route.path("/garbage-tank/**")
                                .uri("lb://GARBAGE-TANK"))
                .route("waste-truck",
                        route -> route.path("/waste-truck/**")
                                .uri("lb://WASTE-TRUCK"))
                .build();
    }
}
